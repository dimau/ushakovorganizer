import React from 'react';

export default class Task extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: props.text,
      checked: props.checked
    };
  }
  
  render() {
    return (
      <div className='task'>
        <input type='checkbox' className='task__checkbox' />
        <span className='task__text'> {this.state.text}</span>
      </div>
    );
  }
}
