import React from 'react';

export default class Header extends React.Component {
    render() {
        return (
            <div className="header">
                <h1>All my tasks</h1>
            </div>
        );
    }
}