import React from 'react';

export default class AddTask extends React.Component {
  constructor(props) {
    super(props)
    this.handleAddNewTask = this.handleAddNewTask.bind(this);
  }

  handleAddNewTask(event) {
    event.preventDefault();

    const newTaskText = document.forms.addNewTaskForm.elements.addNewTaskText.value;
    document.forms.addNewTaskForm.elements.addNewTaskText.value = '';
    this.props.onAddNewTask({ text: newTaskText });
  }

  render() {
    return (
      <form name='addNewTaskForm' className='add-task'>
        <input type='text' name='addNewTaskText' className='add-task__text'></input>
        <input type='submit' value='Add' autoFocus onClick={this.handleAddNewTask} />
      </form>
    );
  }
}
