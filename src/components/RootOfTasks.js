import React from 'react';
import Task from './Task';
import AddTask from './AddTask';

export default class RootOfTasks extends React.Component {
  constructor(props) {
    super(props);
    this.onAddNewTask = this.onAddNewTask.bind(this);
    this.focusOnNextTask = this.focusOnNextTask.bind(this);
    this.state = {
      tasks: [],
      selectedTask: null
    };
  }

  onAddNewTask(newTask) {
    this.setState((prevState) => {
      return {
        tasks: [newTask].concat(prevState.tasks)
      };
    });
  }

  componentDidMount() {
    const json = localStorage.getItem('tasks');
    if (json !== null) {
      const tasks = JSON.parse(json);
      this.setState(() => ({ tasks }));
    }
  }

  componentDidUpdate() {
    const json = JSON.stringify(this.state.tasks);
    localStorage.setItem('tasks', json);
  }

  focusOnNextTask() {
    if (this.state.selectedTask === null) {
      this.state.selectedTask = 0;
    } else if (this.state.selectedTask == tasks.length - 1) {
      this.state.selectedTask = 0;
    } else {
      this.state.selectedTask += 1;
    }
  }

  render() {
    return (
      <div className='root-of-tasks' onKeyPress={this.focusOnNextTask}>
        <AddTask onAddNewTask={this.onAddNewTask} />
        {
          this.state.tasks.map((task, taskIndex) => (
            <Task
              text={task.text}
              checked={task.checked}
              selected={taskIndex == this.state.selectedTask ? true : false}
              key={task.text}
            />
          ))
        }
      </div>
    );
  }
}
