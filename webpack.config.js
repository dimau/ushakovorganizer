const path = require('path');

module.exports = {
  mode: "development",
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "public/"),
    filename: "main.js"
  },
  module: {
    rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
    }, {
      test: /\.s?css$/,
      use: [
        'style-loader',
        'css-loader',
        'sass-loader'
      ]
    }]
  },
  devServer: {
    contentBase: path.resolve(__dirname, "public/"),
    historyApiFallback: true,
    port: 3001
  },
  devtool: 'cheap-module-eval-source-map'
}
